# Basketball Score Prediction Neural Network App - Milestone 6
Utilizing Python Flask we are creating an app that helps a user use the model cretaed in the previous milestones.

## Jupyter Notebook
The notebook provided contains the code for generating the tensorflowjs library

## Initialize Project
To run project, inside directiry run "pip install -r requirements.txt" to install required files. 

Once libraries are installed you can run "python(3) app.py". This command will launch a local server that you can access the app on the browser using the port in the output.