let model;
let message;
(async function () {
    model = await tf.loadLayersModel(model_url);
    console.log("Model Loaded")

})();

async function get_predictions() {
    home_team = $("#home-team").val()
    away_team = $("#away-team").val()

    home_row = []
    away_row = []

    for(var i = 0; i < home.length; i++)
    {
        if(home[i].HomeTeam == home_team)
        {
            home_row = home[i];
            break;
        }
    }

    for(var i = 0; i < away.length; i++)
    {
        if(away[i].AwayTeam == away_team)
        {
            away_row = away[i];
            break;
        }
    }

    console.log(home_row)
    console.log(away_row)
    
    const a = tf.tensor([[home_row['HomeScoreAverage'], home_row['HomeDefenseAverage'],away_row['AwayScoreAverage'], away_row['AwayDefenseAverage']]]);
    let predictions = await model.predict(a);
    const value = predictions.dataSync()[0]

    if(value > 0){
        message = home_team+" is going to win!";
        console.log(home_team+" is going to win!")
    }
    else{
        message = away_team+" is going to win!"
        console.log(away_team+" is going to win!")
    }

    $("#message").html(message);
    
}