from flask import Flask, request, render_template, url_for
import pandas as pd

app = Flask(__name__)

@app.route('/')
def rootpage():
    df = pd.read_json("teams.json")
    home = pd.read_json("home.json")
    away = pd.read_json("away.json")
    return render_template("index.html", teams=df.team.sort_values(), home=home.to_json(orient="records"),  away=away.to_json(orient="records"),model_url=url_for('static', filename='model.json'))

app.run(debug=True)
