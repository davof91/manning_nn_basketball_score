# Neural Network Baskeball Score Prediction - Manning LiveProject

This repo will contain all the notebooks for the class. The ones that say Answer are the onces provided by the class and the other one is the one developed by me.

## Milestone 1
Practice deep learning by creating a NN utilizing keras to convert from Celsius to Farenheit. 

## Milestone 2 
Clean data from NCAA and create columns for the NN.

## Milestone 3
Create a neural network using Keras using the data.

## Milestone 4
Overfit the model to undersrtand how a model overfits to prevent it in the future.

## Milestone 5
Practice tunning the parameters to make the network the most efficient.

## Milestone 6
Deploying a network with a Web App

## Further Reader on NN in Sports
Football(Soccer) - https://www.researchgate.net/publication/334415630_Football_Result_Prediction_by_Deep_Learning_Algorithms

Football (NFL) - http://cs230.stanford.edu/projects_winter_2020/reports/32263160.pdf
